from CP_ABE import *
import json


def main_setup():
    groupObj = PairingGroup('SS512')
    cpabe = CPabe_BSW07(groupObj)
    hyb_abe = HybridABEnc(cpabe, groupObj)
    (pk, mk) = hyb_abe.setup()
    return hyb_abe,groupObj,cpabe, pk, mk


hyb_abe,groupObj,cpabe, pk, mk = main_setup()

