import socket
from threading import Thread
import json
from encrypt_file import enc_file
from decrypt_file import dec_file
import setup
from socket_utils import get_file_md5, get_file_payload
from socket_send_files import send_json_data
from os import path, remove

ENCRYPTED_FILES_DIR = r"/home/gabi/Desktop/abe_server/encrypted_files"

MAX_BUFF_SIZE = 2 * 10 ** 6


class client(Thread):
    def __init__(self, socket, address):
        Thread.__init__(self)
        self.sock = socket
        self.addr = address
        self.start()

    def run(self):
        received_data_str = ""
        received_json = self.sock.recv(MAX_BUFF_SIZE)
        while received_json:
            received_data_str = received_data_str + received_json.decode()
            received_json = self.sock.recv(MAX_BUFF_SIZE)

        received_data = json.loads(received_data_str)
        print(received_data)

        filename = received_data["filename"]

        if received_data["to_be_encrypted"] == True:
            file_content = received_data["file_content"]
            access_policy = received_data["policy"]

            with open(filename, "wb") as fd:
                fd.write(bytes(file_content))

            file_save_path = enc_file(pk=setup.pk, mk=setup.mk, file_path=filename, access_policy=access_policy)

            bool_is_encrypted = True
        else:

            user_attributes = ["STUDENT"] ##### HARDCODED

            sk = setup.hyb_abe.keygen(setup.pk, setup.mk, user_attributes)

            enc_file_path = path.join(ENCRYPTED_FILES_DIR, filename)

            file_save_path = dec_file(pk=setup.pk, sk=sk, enc_file_path=enc_file_path, dec_file_path=filename)

            bool_is_encrypted = False

        # remove(filename)

        to_send_data = dict()
        to_send_data["filename"] = filename
        to_send_data["file_content"] = get_file_payload(file_save_path)
        to_send_data["file_md5"] = get_file_md5(file_save_path)
        to_send_data["encrypted"] = bool_is_encrypted

        to_send_json = json.dumps(to_send_data)
        send_json_data(to_send_json)



def main():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    host = "192.168.100.25"
    port = 8000
    s.bind((host, port))
    s.listen(5)

    while True:
        client_socket, client_address = s.accept()
        print(client_address)
        print(client_socket)
        client(client_socket, client_address)


if __name__ == '__main__':
    main()
