from CP_ABE import *
import setup
import pickle
from os import path

ENCRYPTED_FILES_DIR = r"/home/gabi/Desktop/abe_server/encrypted_files"


def enc_file(pk, mk, file_path, access_policy):
    with open(file_path, "rb") as fd:
        content = fd.read()

    ciphertext = setup.hyb_abe.encrypt(pk, content, access_policy)

    print(ciphertext)

    ciphertext["c1"]["C"] = setup.groupObj.serialize(ciphertext["c1"]["C"])
    for key in ciphertext["c1"]["Cy"]:
        ciphertext["c1"]["Cy"][key] = setup.groupObj.serialize(ciphertext["c1"]["Cy"][key])

    ciphertext["c1"]["C_tilde"] = setup.groupObj.serialize(ciphertext["c1"]["C_tilde"])
    for key in ciphertext["c1"]["Cyp"]:
        ciphertext["c1"]["Cyp"][key] = setup.groupObj.serialize(ciphertext["c1"]["Cyp"][key])

    # print(ciphertext)

    file_save_path = path.join(ENCRYPTED_FILES_DIR, file_path)

    with open(file_save_path, "wb") as fd:
        pickle.dump(ciphertext, fd)

    return file_save_path