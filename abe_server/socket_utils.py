from hashlib import md5
import json

def get_file_md5(file):
    with open(file, "rb") as fd:
        chunk = fd.read(512)
        file_md5 = md5()

        while chunk:
            file_md5.update(chunk)
            chunk = fd.read(512)

    return file_md5.hexdigest()


def update_json(file, policy):
    with open(JSON_FILE, "r") as fd:
        file_dict = json.load(fd)

    file_md5 = get_file_md5(file)

    file_dict[file_md5] = policy

    with open(JSON_FILE, "w") as fd:
        json.dump(file_dict, fd)


def get_file_payload(filename):
    data = []
    with open(filename, "rb") as fd:
        chunk = fd.read(512)
        while chunk:
            data += chunk
            chunk = fd.read(512)
    return data