from CP_ABE import *
import setup
import pickle
from os import path

DECRYPTED_FILES_DIR = r"/home/gabi/Desktop/abe_server/decrypted_files"


def dec_file(pk, sk, enc_file_path, dec_file_path):
    with open(enc_file_path, "rb") as fd:
        ciphertext2 = pickle.load(fd)

        ciphertext2["c1"]["C"] = setup.groupObj.deserialize(ciphertext2["c1"]["C"])
        for key in ciphertext2["c1"]["Cy"]:
            ciphertext2["c1"]["Cy"][key] = setup.groupObj.deserialize(ciphertext2["c1"]["Cy"][key])

        ciphertext2["c1"]["C_tilde"] = setup.groupObj.deserialize(ciphertext2["c1"]["C_tilde"])
        for key in ciphertext2["c1"]["Cyp"]:
            ciphertext2["c1"]["Cyp"][key] = setup.groupObj.deserialize(ciphertext2["c1"]["Cyp"][key])

        # print(ciphertext2)
        decrypted_content = setup.hyb_abe.decrypt(pk, sk, ciphertext2)

    file_save_path = path.join(DECRYPTED_FILES_DIR, dec_file_path)

    with open(file_save_path, "wb") as fd:
        fd.write(decrypted_content)

    return file_save_path