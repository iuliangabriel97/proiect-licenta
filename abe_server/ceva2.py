from charm.toolbox.ABEnc import ABEnc
from charm.schemes.abenc.abenc_bsw07 import CPabe_BSW07
from charm.toolbox.pairinggroup import PairingGroup, GT
from charm.toolbox.symcrypto import AuthenticatedCryptoAbstraction
from charm.core.math.pairing import hashPair as sha2
from math import ceil

debug = False


class HybridABEnc(ABEnc):

    def __init__(self, scheme, groupObj):
        ABEnc.__init__(self)
        global abenc
        # check properties (TODO)
        abenc = scheme
        self.group = groupObj

    def setup(self):
        return abenc.setup()

    def keygen(self, pk, mk, object):
        return abenc.keygen(pk, mk, object)

    def encrypt(self, pk, M, object):
        key = self.group.random(GT)
        c1 = abenc.encrypt(pk, key, object)
        # instantiate a symmetric enc scheme from this key
        cipher = AuthenticatedCryptoAbstraction(sha2(key))
        c2 = cipher.encrypt(M)
        return {'c1': c1, 'c2': c2}

    def decrypt(self, pk, sk, ct):
        c1, c2 = ct['c1'], ct['c2']
        key = abenc.decrypt(pk, sk, c1)
        if key is False:
            raise Exception("failed to decrypt!")
        cipher = AuthenticatedCryptoAbstraction(sha2(key))
        return cipher.decrypt(c2)


def main():
    groupObj = PairingGroup('SS512')
    cpabe = CPabe_BSW07(groupObj)
    hyb_abe = HybridABEnc(cpabe, groupObj)
    access_policy = '((four or three) and (two or one))'
    full_dec = ""
    original_message = ""
    (pk, mk) = hyb_abe.setup()
    sk = hyb_abe.keygen(pk, mk, ['ONE', 'TWO', 'THREE'])
    with open("lorem_ipsum.txt", 'r') as fd:
        message = fd.read(32)
        while message:
            original_message += message
            ct = hyb_abe.encrypt(pk, message, access_policy)
            mdec = hyb_abe.decrypt(pk, sk, ct)
            # print(mdec)
            full_dec += mdec.decode()
            message = fd.read(32)
    print(full_dec)
    print("#######################################")
    print(original_message)

    assert full_dec == original_message, "Failed Decryption!!!"
    print("Successful Decryption!!!")


if __name__ == "__main__":
    main()
