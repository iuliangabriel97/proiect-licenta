import socket

socket_server_host = "192.168.100.17"
socket_server_port = 5000

MAX_BUFF_SIZE = 2 * 10 ** 6


def send_json_data(data):
    s = socket.socket()
    host = socket_server_host
    port = socket_server_port
    c = s.connect_ex((host, port))
    byte_array_data = bytearray(data, "utf-8")
    for i in range(0, len(byte_array_data), MAX_BUFF_SIZE):
        s.send(byte_array_data[i:i + MAX_BUFF_SIZE])
    s.close()