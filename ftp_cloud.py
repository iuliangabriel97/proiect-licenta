import socket
from ftp_cloud_config import cloud_host, cloud_port

MAX_BUFF_SIZE = 2 * 10 ** 6

def send_file(file):
    s = socket.socket()
    host = cloud_host
    port = cloud_port
    c = s.connect((host, port))

    with open(file, "rb") as fd:
        chunk = fd.read(512)
        while chunk:
            s.send(chunk)
            chunk = fd.read()

    s.close()


def send_json_data(data):
    s = socket.socket()
    host = cloud_host
    port = cloud_port
    c = s.connect_ex((host, port))
    byte_array_data = bytearray(data, "utf-8")
    for i in range(0, len(byte_array_data), MAX_BUFF_SIZE):
        s.send(byte_array_data[i:i + MAX_BUFF_SIZE])
    s.close()


if __name__ == '__main__':
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    host = "192.168.100.25"

    port = 8000

    print("Trying to connect to {}:{}".format(host, port))
    c = s.connect_ex((host, port))
    print(c)
    send_file(r"C:\Users\gcrisnuta\Desktop\ProiectLicenta\Notes.txt")
    s.close()
    print("Finish")
