import json
import random
import string
import db_config
from pymongo import MongoClient
from userclass import User
from bson import objectid

host = db_config.host
port = db_config.port

client = MongoClient(host, port)
db = client['admin']

names_txt = r"names.txt"
names_json = r'names.json'
users_json = r'users.json'


def get_db():
    host = db_config.host
    port = db_config.port

    client = MongoClient(host, port)
    db = client['admin']
    return db


def get_users_collection():
    db = get_db()
    return db['Users']


def generate_random_password():
    password_length = 10
    password = ''.join(
        random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits) for _ in range(password_length))
    return password


def generate_random_username():
    name_length = 10
    name = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(name_length))
    return name


def generate_random_users_json():
    no_of_users = 50
    users = list()

    for _ in range(no_of_users):
        username = generate_random_username()
        password = generate_random_password()

        user = dict()
        user['username'] = username
        user['password'] = password
        users.append(user)

    # print(users)

    with open(users_json, 'w') as fd:
        json.dump(users, fd)


def create_users_table():
    db.create_collection("Users")


def delete_users_table():
    db.drop_collection("Users")


def create_user(username, password):
    new_user = User(username, password)
    db['Users'].insert_one(new_user.__dict__)


def populate_users_table():
    with open(users_json, 'r') as fd:
        users = json.load(fd)

    for user in users:
        username = user['username']
        password = user['password']
        create_user(username, password)


def existing_username(username):
    users_collection = get_users_collection()
    user = users_collection.find_one({'username': username})
    if user:
        return True

    return False


def get_password_hash(username):
    users_collection = get_users_collection()
    user = users_collection.find_one({'username': username})
    if user:
        return user['password_hash']

    return None


def get_user_by_id(user_id):
    users_collection = get_users_collection()
    user_object_id = objectid.ObjectId(user_id)
    # print("type(user_object_id)={}".format(type(user_object_id)))
    # print("user_object_id={}".format(user_object_id))
    user = users_collection.find_one({'_id': user_object_id})
    # print("user={}".format(user))
    if user:
        # print(user['username'], user['password_hash'])
        user_object = User()
        user_object.username = user['username']
        user_object.password_hash = user['password_hash']
        user_object.id = user_id
        return user_object
    print("N am gasit user cu id {}".format(user_id))
    return None


def get_object_id_by_username(username):
    users_collection = get_users_collection()
    user_object_id = users_collection.find_one({'username': username})['_id']
    # print("user_object_id={}".format(user_object_id))
    return user_object_id


if __name__ == '__main__':
    users_collection = get_users_collection()
    user = users_collection.find_one({'username': 'gabi'})
    print(user)

    user_id = user['_id']

    print(users_collection.find_one({'_id': user_id}))

    # delete_users_table()
    #
    # create_users_table()
    #
    # generate_random_users_json()
    #
    # populate_users_table()
    #
    # create_user(username='gabi', password='gabi')
