import server_config
from server_config import allowed_file
from flask import Flask, flash, redirect, render_template, request, send_file, send_from_directory
import userclass
from flask_login import LoginManager, current_user, login_user, login_required, UserMixin, logout_user
from db_utils import existing_username, get_password_hash, get_user_by_id, get_object_id_by_username
from werkzeug.security import check_password_hash
from werkzeug.utils import secure_filename
from os import path
import os
import json
from ftp_cloud import send_file, send_json_data
from server_utils import get_file_md5, get_file_payload
import time

SERVER_HOST = server_config.SERVER_HOST
SERVER_PORT = server_config.SERVER_PORT
UPLOAD_FOLDER = server_config.UPLOAD_FOLDER
MAX_CONTENT_LENGTH = server_config.MAX_CONTENT_LENGTH
JSON_FILE = server_config.JSON_FILE
MAX_BUFF_SIZE = 2 * 10 ** 6
TEMP_DECRYPTED_FOLDER = server_config.TEMP_DECRYPTED_FOLDER

app = Flask(__name__)

app.secret_key = server_config.SERVER_SECRET_KEY
app.config['SESSION_TYPE'] = 'memcached'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = MAX_CONTENT_LENGTH

login_manager = LoginManager()
login_manager.login_view = '/login'

login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id):
    # print("user_id={}".format(user_id))
    # print("type(user_id)={}".format(type(user_id))) #str
    return get_user_by_id(user_id)  # user_id este None


@app.route("/")
def index():
    if not current_user.is_authenticated:
        return render_template('login')
    else:
        return redirect("/logged-in")


@app.route("/login", methods=['GET', 'POST'])
def login():
    input_username = request.form['username']
    input_password = request.form['password']
    # print(input_username, input_password)

    if existing_username(input_username):
        if check_password_hash(get_password_hash(input_username), input_password):
            user_id = get_object_id_by_username(input_username)
            user = userclass.User(username=input_username, password=input_password, id=str(user_id))
            login_user(user)
            # print(login_user(user))
            flash("Successfully logged it!")
            return redirect("/logged-in")

    flash("Wrong username/password")
    return redirect("/")


@app.route("/logged-in", methods=['GET', 'POST'])
@login_required
def main_page():
    return render_template('logout.html')


@app.route('/logout', methods=["GET", "POST"])
@login_required
def logout():
    logout_user()
    return redirect("/")


@app.route('/upload', methods=["GET", "POST"])
@login_required
def upload():
    if request.method == 'POST':
        flash_message = "Succesfully uploaded"

        if 'file' not in request.files:
            flash_message = "No file part!"

        file = request.files['file']
        policy = request.form["policy"]

        if file.filename == '':
            flash_message = "No selected file!"

        if file:
            if not allowed_file(file.filename):
                flash_message = "File extension not allowed!"
            else:
                filename = secure_filename(file.filename)
                file_save_path = path.join(app.config['UPLOAD_FOLDER'], filename)
                file.save(file_save_path)
                # print(file_save_path)

                file_md5 = get_file_md5(file_save_path)
                file_size = os.path.getsize(file_save_path)

                to_send_data = dict()
                to_send_data["filename"] = filename
                file_content = get_file_payload(file_save_path)
                to_send_data["size"] = os.path.getsize(file_save_path)
                to_send_data["md5"] = file_size
                to_send_data["policy"] = policy
                to_send_data["file_content"] = file_content
                to_send_data["to_be_encrypted"] = True

                to_send_json = json.dumps(to_send_data)
                print(to_send_json)
                send_json_data(to_send_json)
                os.remove(file_save_path)

                flash_message = "File uploaded!"

        flash(flash_message)
        return redirect("/logged-in")


def get_files_from_directory():
    file_paths_list = list()
    for root, dirs, files in os.walk(UPLOAD_FOLDER):
        for file in files:
            file_path = path.join(UPLOAD_FOLDER, file)
            file_paths_list.append(file)
    return file_paths_list


@app.route('/download/<path>')
@login_required
def download(path=None):
    print(path)
    if path is None:
        pass
        return ("Path is none")
    try:
        full_path = os.path.join(UPLOAD_FOLDER, path)
        filename = path
        file_md5 = get_file_md5(full_path)
        file_size = os.path.getsize(full_path)

        to_send_data = dict()
        to_send_data["filename"] = filename
        file_content = get_file_payload(full_path)
        to_send_data["size"] = os.path.getsize(full_path)
        to_send_data["md5"] = file_size
        to_send_data["file_content"] = file_content
        to_send_data["to_be_encrypted"] = False

        to_send_json = json.dumps(to_send_data)
        send_json_data(to_send_json)

        decrypted_file_path = os.path.join(TEMP_DECRYPTED_FOLDER, filename)
        print(decrypted_file_path)

        return send_from_directory(TEMP_DECRYPTED_FOLDER, filename, as_attachment=True)
    except Exception as e:
        pass
        return str(e)


@app.route('/download_files', methods=["GET", "POST"])
@login_required
def download_files():
    file_paths_list = get_files_from_directory()
    return render_template("download_files.html", files=file_paths_list)


if __name__ == '__main__':
    app.run(host=SERVER_HOST, port=SERVER_PORT)
