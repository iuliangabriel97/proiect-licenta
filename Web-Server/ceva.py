from db_utils import get_users_collection
from werkzeug.security import check_password_hash

if __name__ == '__main__':
    users_collection = get_users_collection()
    user = users_collection.find_one({"username": r"6tvrn7raus"})
    psswd_hash = user['password_hash']
    input_password = r"Ni8ZAlA7Nh"
    print(check_password_hash(psswd_hash, input_password))
