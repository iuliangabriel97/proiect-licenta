import sys

sys.path.insert(0, r'C:\Users\gcrisnuta\Desktop\ProiectLicenta\Web-Server')

SERVER_HOST = '0.0.0.0'
SERVER_PORT = 4000
SERVER_SECRET_KEY = "KEEPITSECRET"
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
UPLOAD_FOLDER = r'C:\Users\gcrisnuta\Desktop\ProiectLicenta\Downloads'
JSON_FILE = r'C:\Users\gcrisnuta\Desktop\ProiectLicenta\Downloads\file_dict.json'
MAX_CONTENT_LENGTH = 16 * 1024 * 1024
TEMP_DECRYPTED_FOLDER = r"C:\Users\gcrisnuta\Desktop\ProiectLicenta\TEMP_DECRYPTED"


def allowed_file(filename):
    return filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

if __name__ == '__main__':
    print(allowed_file(""))
