from hashlib import md5
import json
from server_config import JSON_FILE


def get_file_md5(file):
    with open(file, "rb") as fd:
        chunk = fd.read(512)
        file_md5 = md5()

        while chunk:
            file_md5.update(chunk)
            chunk = fd.read(512)

    return file_md5.hexdigest()


def update_json(file, policy):
    with open(JSON_FILE, "r") as fd:
        file_dict = json.load(fd)

    file_md5 = get_file_md5(file)

    file_dict[file_md5] = policy

    with open(JSON_FILE, "w") as fd:
        json.dump(file_dict, fd)


def validate_policy(policy):
    no_open_paranthesis = no_closed_paranthesis = 0

    for c in policy:
        if c == ')':
            no_closed_paranthesis += 1

        if c == '(':
            no_open_paranthesis += 1

        if no_closed_paranthesis > no_open_paranthesis:
            return -1

    if no_open_paranthesis != no_closed_paranthesis:
        return -2

def get_file_payload(filename):
    data = []
    with open(filename, "rb") as fd:
        chunk = fd.read(512)
        while chunk:
            data += chunk
            chunk = fd.read(512)
    return data