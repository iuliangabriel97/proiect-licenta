import socket
from threading import Thread
import json
import os

MAX_BUFF_SIZE = 2 * 10 ** 6
TEMP_DECRYPTED_DIR = r"TEMP_DECRYPTED"
DOWNLOADS_DIR = r"C:\Users\gcrisnuta\Desktop\ProiectLicenta\Downloads"


class client(Thread):
    def __init__(self, socket, address):
        Thread.__init__(self)
        self.sock = socket
        self.addr = address
        self.start()

    def run(self):
        received_data_str = ""
        received_json = self.sock.recv(MAX_BUFF_SIZE)
        while received_json:
            received_data_str = received_data_str + received_json.decode()
            received_json = self.sock.recv(MAX_BUFF_SIZE)
            # print(received_data_str)
        received_data = json.loads(received_data_str)
        # print(received_data)
        # print(type(received_data))
        filename = received_data["filename"]
        file_content = received_data["file_content"]
        bool_is_encrypted = received_data["encrypted"]

        # print(file_content)
        print(bool_is_encrypted)
        if bool_is_encrypted:
            file_save_path = os.path.join(DOWNLOADS_DIR, filename)
        else:
            file_save_path = os.path.join(TEMP_DECRYPTED_DIR, filename)
        print(file_save_path)

        with open(file_save_path, "wb") as fd:
            fd.write(bytes(file_content))


def main():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    host = "192.168.100.17"
    port = 5000
    s.bind((host, port))
    s.listen(5)

    while True:
        client_socket, client_address = s.accept()
        print((client_socket, client_address))
        # print(client_address)
        client(client_socket, client_address)


if __name__ == '__main__':
    main()
